﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shopping
{
    internal class Producto
    {
        private int id { get; set; }
        private string nombre { get; set; }
        private string barCode { get; set; }
        private string rutaImagen { get; set; }
        private Double precio { get; set; }

        public Producto(string nombre, string barCode, Double precio)
        {
            this.nombre = nombre;
            this.barCode = barCode;
            this.rutaImagen = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Imagenes\\"+barCode+".jpg");
            this.precio = precio;
        }

        public int getId() { return id; }
        public string getNombre() {  return nombre; }
        public string getBarCode() { return barCode; }
        public string getRutaImagen() {  return rutaImagen; }
        public Double getPrecio() { return precio; }

        public void setNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public void setRutaImagen(string rutaImagen)
        {
            this.rutaImagen = rutaImagen;
        }
        public void setPrecio(Double precio)
        {
            this.precio= precio;
        }

    }
}
