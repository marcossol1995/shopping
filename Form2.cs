﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Shopping
{
    public partial class Form2 : Form
    {
        private string v1;
        private string v2;

        public string codigo { get; set; }
        public string nombre { get; set; }
        public string rutaImagen { get; set; }
        public string precio { get; set; }
        public bool aceptarPulsado { get; private set; }
        public bool esModificacion { get; private set; }
        public Form2()
        {
            InitializeComponent();
            this.aceptarPulsado = false;
            this.esModificacion = false;
            tbPrecio.KeyPress += textBoxPrecio_KeyPress;
            tbPrecio.TextChanged += textBoxPrecio_TextChanged;
            
        }

        public Form2(string codigo,string nombre,string precio,string rutaImagen)
        {
            InitializeComponent();
            this.esModificacion = true;
            this.codigo = codigo;
            tbCode.Text = this.codigo;
            this.nombre = nombre;
            tbNombre.Text = this.nombre;
            this.precio = precio;
            tbPrecio.Text = this.precio;
            this.rutaImagen = rutaImagen;
            pbImagen.Image = Image.FromFile(this.rutaImagen);
            this.tbCode.Enabled = false;
        }

        private void btnImagen_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Archivos de imagen|*.jpg;*.jpeg;*.png;*.gif;*.bmp";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                rutaImagen = openFileDialog.FileName;
                // Muestra la imagen seleccionada en el PictureBox
                pbImagen.Image = Image.FromFile(rutaImagen);
            }
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            // Cargo las variables publicas del producto cargado
            codigo = tbCode.Text;
            nombre = tbNombre.Text;
            precio = tbPrecio.Text;
            // Cierro formulario
            this.aceptarPulsado = true;
            pbImagen.Dispose();
            this.Close();
        }

        private void textBoxPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Verifica si la tecla presionada es un número o una tecla de control
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                // Si no es un número ni una tecla de control, suprime la tecla presionada
                e.Handled = true;
            }
        }

        private void textBoxPrecio_TextChanged(object sender, EventArgs e)
        {
            // Verifica si el contenido del TextBox es un número válido
            if (!decimal.TryParse(tbPrecio.Text, out decimal precio) || precio <= 0)
            {
                // Si no es un número válido o es menor o igual a cero, puedes tomar acciones, como mostrar un mensaje de error
                //MessageBox.Show("Ingrese un valor numérico válido mayor que cero.");
                // También puedes limpiar el TextBox o tomar otras acciones según tus necesidades.
                tbPrecio.Clear();
            }
        }
    }
}
