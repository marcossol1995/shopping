﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Shopping
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnAgregar = new System.Windows.Forms.Button();
            this.dgvProductos = new System.Windows.Forms.DataGridView();
            this.colCodigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrecio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colCompra = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.pbImagen = new System.Windows.Forms.PictureBox();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.lbMonto = new System.Windows.Forms.Label();
            this.tbMonto = new System.Windows.Forms.TextBox();
            this.btnComprar = new System.Windows.Forms.Button();
            this.lbDeuda = new System.Windows.Forms.Label();
            this.tbDeuda = new System.Windows.Forms.TextBox();
            this.btnCdeuda = new System.Windows.Forms.Button();
            this.btnAdeudar = new System.Windows.Forms.Button();
            this.btnHistorial = new System.Windows.Forms.Button();
            this.btnPagar = new System.Windows.Forms.Button();
            this.productoBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.productoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.form1BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.form1BindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.tbPaga = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnAgregar
            // 
            this.btnAgregar.Location = new System.Drawing.Point(21, 13);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(75, 23);
            this.btnAgregar.TabIndex = 1;
            this.btnAgregar.Text = "Agregar";
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.button1_Click);
            // 
            // dgvProductos
            // 
            this.dgvProductos.AllowUserToAddRows = false;
            this.dgvProductos.AllowUserToResizeColumns = false;
            this.dgvProductos.AllowUserToResizeRows = false;
            this.dgvProductos.BackgroundColor = System.Drawing.SystemColors.ButtonShadow;
            this.dgvProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProductos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colCodigo,
            this.colNombre,
            this.colPrecio,
            this.colCompra});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvProductos.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvProductos.Location = new System.Drawing.Point(21, 62);
            this.dgvProductos.MultiSelect = false;
            this.dgvProductos.Name = "dgvProductos";
            this.dgvProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProductos.Size = new System.Drawing.Size(394, 402);
            this.dgvProductos.TabIndex = 3;
            this.dgvProductos.TabStop = false;
            this.dgvProductos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvProductos_CellDoubleClick);
            // 
            // colCodigo
            // 
            this.colCodigo.DataPropertyName = "barCode";
            this.colCodigo.FillWeight = 300F;
            this.colCodigo.HeaderText = "Codigo";
            this.colCodigo.Name = "colCodigo";
            this.colCodigo.ReadOnly = true;
            // 
            // colNombre
            // 
            this.colNombre.DataPropertyName = "nombre";
            this.colNombre.FillWeight = 300F;
            this.colNombre.HeaderText = "Nombre";
            this.colNombre.Name = "colNombre";
            this.colNombre.ReadOnly = true;
            // 
            // colPrecio
            // 
            this.colPrecio.DataPropertyName = "precio";
            dataGridViewCellStyle1.NullValue = null;
            this.colPrecio.DefaultCellStyle = dataGridViewCellStyle1;
            this.colPrecio.FillWeight = 300F;
            this.colPrecio.HeaderText = "Precio";
            this.colPrecio.Name = "colPrecio";
            this.colPrecio.ReadOnly = true;
            // 
            // colCompra
            // 
            dataGridViewCellStyle2.NullValue = null;
            this.colCompra.DefaultCellStyle = dataGridViewCellStyle2;
            this.colCompra.FillWeight = 300F;
            this.colCompra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.colCompra.HeaderText = "Compra";
            this.colCompra.MaxDropDownItems = 10;
            this.colCompra.Name = "colCompra";
            this.colCompra.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colCompra.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colCompra.Width = 50;
            // 
            // pbImagen
            // 
            this.pbImagen.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pbImagen.Location = new System.Drawing.Point(438, 62);
            this.pbImagen.Name = "pbImagen";
            this.pbImagen.Size = new System.Drawing.Size(320, 320);
            this.pbImagen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pbImagen.TabIndex = 3;
            this.pbImagen.TabStop = false;
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(102, 13);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 2;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // lbMonto
            // 
            this.lbMonto.AutoSize = true;
            this.lbMonto.Location = new System.Drawing.Point(438, 407);
            this.lbMonto.Name = "lbMonto";
            this.lbMonto.Size = new System.Drawing.Size(66, 13);
            this.lbMonto.TabIndex = 4;
            this.lbMonto.Text = "Monto total: ";
            // 
            // tbMonto
            // 
            this.tbMonto.Enabled = false;
            this.tbMonto.Location = new System.Drawing.Point(510, 404);
            this.tbMonto.Name = "tbMonto";
            this.tbMonto.Size = new System.Drawing.Size(100, 20);
            this.tbMonto.TabIndex = 5;
            // 
            // btnComprar
            // 
            this.btnComprar.Location = new System.Drawing.Point(441, 440);
            this.btnComprar.Name = "btnComprar";
            this.btnComprar.Size = new System.Drawing.Size(75, 23);
            this.btnComprar.TabIndex = 6;
            this.btnComprar.Text = "Comprar";
            this.btnComprar.UseVisualStyleBackColor = true;
            this.btnComprar.Click += new System.EventHandler(this.btnComprar_Click);
            // 
            // lbDeuda
            // 
            this.lbDeuda.AutoSize = true;
            this.lbDeuda.Location = new System.Drawing.Point(606, 15);
            this.lbDeuda.Name = "lbDeuda";
            this.lbDeuda.Size = new System.Drawing.Size(45, 13);
            this.lbDeuda.TabIndex = 7;
            this.lbDeuda.Text = "Deuda: ";
            // 
            // tbDeuda
            // 
            this.tbDeuda.Enabled = false;
            this.tbDeuda.Location = new System.Drawing.Point(657, 12);
            this.tbDeuda.Name = "tbDeuda";
            this.tbDeuda.Size = new System.Drawing.Size(100, 20);
            this.tbDeuda.TabIndex = 8;
            // 
            // btnCdeuda
            // 
            this.btnCdeuda.Location = new System.Drawing.Point(500, 12);
            this.btnCdeuda.Name = "btnCdeuda";
            this.btnCdeuda.Size = new System.Drawing.Size(100, 20);
            this.btnCdeuda.TabIndex = 9;
            this.btnCdeuda.Text = "Cancelar deuda";
            this.btnCdeuda.UseMnemonic = false;
            this.btnCdeuda.UseVisualStyleBackColor = true;
            this.btnCdeuda.Click += new System.EventHandler(this.btnCdeuda_Click);
            // 
            // btnAdeudar
            // 
            this.btnAdeudar.Location = new System.Drawing.Point(535, 440);
            this.btnAdeudar.Name = "btnAdeudar";
            this.btnAdeudar.Size = new System.Drawing.Size(75, 23);
            this.btnAdeudar.TabIndex = 10;
            this.btnAdeudar.Text = "Adeudar";
            this.btnAdeudar.UseVisualStyleBackColor = true;
            this.btnAdeudar.Click += new System.EventHandler(this.btnAdeudar_Click);
            // 
            // btnHistorial
            // 
            this.btnHistorial.Location = new System.Drawing.Point(500, 38);
            this.btnHistorial.Name = "btnHistorial";
            this.btnHistorial.Size = new System.Drawing.Size(100, 20);
            this.btnHistorial.TabIndex = 11;
            this.btnHistorial.Text = "Historial";
            this.btnHistorial.UseVisualStyleBackColor = true;
            this.btnHistorial.Click += new System.EventHandler(this.btnHistorial_Click);
            // 
            // btnPagar
            // 
            this.btnPagar.Location = new System.Drawing.Point(609, 37);
            this.btnPagar.Name = "btnPagar";
            this.btnPagar.Size = new System.Drawing.Size(42, 22);
            this.btnPagar.TabIndex = 12;
            this.btnPagar.Text = "Pagar deuda";
            this.btnPagar.UseVisualStyleBackColor = true;
            this.btnPagar.Click += new System.EventHandler(this.btnPagar_Click);
            // 
            // productoBindingSource1
            // 
            this.productoBindingSource1.DataSource = typeof(Shopping.Producto);
            // 
            // productoBindingSource
            // 
            this.productoBindingSource.DataSource = typeof(Shopping.Producto);
            // 
            // form1BindingSource
            // 
            this.form1BindingSource.DataSource = typeof(Shopping.Form1);
            // 
            // form1BindingSource1
            // 
            this.form1BindingSource1.DataSource = typeof(Shopping.Form1);
            // 
            // tbPaga
            // 
            this.tbPaga.Location = new System.Drawing.Point(657, 38);
            this.tbPaga.Name = "tbPaga";
            this.tbPaga.Size = new System.Drawing.Size(100, 20);
            this.tbPaga.TabIndex = 13;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(782, 504);
            this.Controls.Add(this.tbPaga);
            this.Controls.Add(this.btnPagar);
            this.Controls.Add(this.btnHistorial);
            this.Controls.Add(this.btnAdeudar);
            this.Controls.Add(this.btnCdeuda);
            this.Controls.Add(this.tbDeuda);
            this.Controls.Add(this.lbDeuda);
            this.Controls.Add(this.btnComprar);
            this.Controls.Add(this.tbMonto);
            this.Controls.Add(this.lbMonto);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.pbImagen);
            this.Controls.Add(this.dgvProductos);
            this.Controls.Add(this.btnAgregar);
            this.Name = "Form1";
            this.Text = "Shopping";
            ((System.ComponentModel.ISupportInitialize)(this.dgvProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbImagen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.productoBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.form1BindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAgregar;
        private BindingSource form1BindingSource;
        private BindingSource form1BindingSource1;
        private BindingSource productoBindingSource;
        private DataGridView dgvProductos;
        private BindingSource productoBindingSource1;
        private PictureBox pbImagen;
        private Button btnEliminar;
        private Label lbMonto;
        private TextBox tbMonto;
        private DataGridViewTextBoxColumn colCodigo;
        private DataGridViewTextBoxColumn colNombre;
        private DataGridViewTextBoxColumn colPrecio;
        private DataGridViewComboBoxColumn colCompra;
        private Button btnComprar;
        private Label lbDeuda;
        private TextBox tbDeuda;
        private Button btnCdeuda;
        private Button btnAdeudar;
        private Button btnHistorial;
        private Button btnPagar;
        private TextBox tbPaga;
    }
}

