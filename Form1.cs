﻿using MySql.Data.MySqlClient;
using MySqlX.XDevAPI;
using MySqlX.XDevAPI.Relational;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using Image = System.Drawing.Image;

namespace Shopping
{
    public partial class Form1 : Form
    {
        List<Producto> listaProductos;
        string rutaDirectorioEjecutable; 
        string subcarpetaImagenes;
        public Form1()
        {            
            InitializeComponent();
            this.Load += Form1_Load;
            //Carpeta de imagenes
            rutaDirectorioEjecutable = AppDomain.CurrentDomain.BaseDirectory;
            subcarpetaImagenes = Path.Combine(rutaDirectorioEjecutable, "Imagenes");
            if (!Directory.Exists(subcarpetaImagenes) || !Directory.EnumerateFiles(subcarpetaImagenes).Any())
            {
                Directory.CreateDirectory(subcarpetaImagenes);
            }
            //Eventos
            dgvProductos.DataBindingComplete += dgvProductos_DataBindingComplete;
            dgvProductos.EditingControlShowing += dgvProductos_EditingControlShowing;
            dgvProductos.SelectionChanged += dgvProductos_SelectionChanged;
            dgvProductos.CellValueChanged += dgvProductos_CellValueChanged;
            dgvProductos.CurrentCellDirtyStateChanged += dgvProductos_CurrentCellDirtyStateChanged;
            //Carga inicial de productos
            listaProductos = new List<Producto>();
            //obtenerProductosDeBase();
            //actualizarDataGridView();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (Form2 formularioAgregar = new Form2())
            {
                formularioAgregar.ShowDialog();
                //Verifica si se ingresaron datos en el formulario de ingreso
                if (esCargaValida(formularioAgregar.nombre,formularioAgregar.codigo,formularioAgregar.precio,formularioAgregar.rutaImagen) && !esRepetido(formularioAgregar.codigo))       
                {
                    Producto nuevoProducto = new Producto(formularioAgregar.nombre, formularioAgregar.codigo, Double.Parse(formularioAgregar.precio));
                    listaProductos.Add(nuevoProducto);
                    try 
                    {
                        File.Copy(formularioAgregar.rutaImagen, subcarpetaImagenes + "\\" + formularioAgregar.codigo + ".jpg", true);
                    }
                    catch (IOException ex)
                    {
                        MessageBox.Show($"Error al copiar la imagen: {ex.Message}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    // Actualiza el DataGridView
                    actualizarDataGridView();
                    insertarProdBase();
                }
                else if (formularioAgregar.aceptarPulsado)
                {
                    MessageBox.Show("Producto invalido o repetido");
                }
            }
        }

        private void actualizarDataGridView()
        {
            Dictionary<int, object> valoresComboBox = ObtenerValoresComboBox();
            dgvProductos.DataSource = null;
            dgvProductos.DataSource = listaProductos;
            int indice = 0;
            foreach (Producto producto in listaProductos)
            {
                dgvProductos.Rows[indice].Cells["colNombre"].Value = producto.getNombre();
                dgvProductos.Rows[indice].Cells["colCodigo"].Value = producto.getBarCode();
                dgvProductos.Rows[indice].Cells["colPrecio"].Value = producto.getPrecio();
                //DataGridViewComboBoxCell comboBoxCellCompra = dgvProductos.Rows[indice].Cells["colCompra"] as DataGridViewComboBoxCell;
                //if (comboBoxCellCompra.Value != null)
                //{
                //    dgvProductos.Rows[indice].Cells["colCompra"].Value = dgvProductos.Rows[indice].Cells["colCompra"].Value;
                //}
                indice++;
            }
            RestaurarValoresComboBox(valoresComboBox);
            dgvProductos.Refresh();
        }
        private Dictionary<int, object> ObtenerValoresComboBox()
        {
            Dictionary<int, object> valoresComboBox = new Dictionary<int, object>();

            // Itera a través de las filas del DataGridView y guarda los valores de la columna de ComboBox
            try
            {
                foreach (DataGridViewRow row in dgvProductos.Rows)
                {
                    // Obtén el producto asociado a la fila
                    Producto producto = row.DataBoundItem as Producto;
                    // Verifica si el producto existe en la lista
                    if (producto != null && listaProductos.Contains(producto))
                    {
                        DataGridViewComboBoxCell comboBoxCell = row.Cells["colCompra"] as DataGridViewComboBoxCell;
                        if (comboBoxCell != null)
                        {
                            valoresComboBox.Add(row.Index, comboBoxCell.Value);
                        }
                    }
                }
            }
            catch { }
            return valoresComboBox;
        }

        private void RestaurarValoresComboBox(Dictionary<int, object> valoresComboBox)
        {
            // Restaura los valores de la columna de ComboBox basados en el diccionario
            foreach (KeyValuePair<int, object> kvp in valoresComboBox)
            {
                int rowIndex = kvp.Key;
                object valorComboBox = kvp.Value;

                DataGridViewComboBoxCell comboBoxCell = dgvProductos.Rows[rowIndex].Cells["colCompra"] as DataGridViewComboBoxCell;
                if (comboBoxCell != null)
                {
                    comboBoxCell.Value = valorComboBox;
                }
            }
        }
        private void insertarProdBase()
        {
            foreach (Producto producto in listaProductos)
            {
                string codigo = producto.getBarCode();
                string nombre = producto.getNombre();
                double precio = producto.getPrecio();

                String sql = "INSERT INTO productos (codigo, nombre, precio) VALUES ('" + codigo + "','" + nombre + "'," + precio.ToString(CultureInfo.InvariantCulture) + ") ON DUPLICATE KEY UPDATE nombre = '" + nombre + "', precio = " + precio.ToString(CultureInfo.InvariantCulture) + "";

                String cadenaConexion = "Database=shopping ; Data Source=localhost ; Port=3306 ; User Id=root ; Password= 123456789;";
                MySqlConnection conexionBD = new MySqlConnection(cadenaConexion);
                conexionBD.Open();

                try
                {
                    MySqlCommand comando = new MySqlCommand(sql, conexionBD);
                    comando.ExecuteNonQuery();
                }
                catch (MySqlException ex)
                {
                    MessageBox.Show("Error MySQL - Código: " + ex.Number + ", Mensaje: " + ex.Message);
                }
                finally { conexionBD.Close(); }            
            }

        }

        private void dgvProductos_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (dgvProductos.CurrentCell is DataGridViewComboBoxCell && e.Control is ComboBox)
            {
                ComboBox comboBox = (ComboBox)e.Control;

                // Ajustar el número de elementos visibles en el ComboBox
                comboBox.IntegralHeight = false;
                comboBox.MaxDropDownItems = 10;
                //
                List<string> numeros = Enumerable.Range(0, 101).Select(i => i.ToString()).ToList();
                colCompra.Items.AddRange(numeros.ToArray());
            }
        }

        private void dgvProductos_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (e.ListChangedType == ListChangedType.Reset)
            {
                List<string> numeros = Enumerable.Range(0, 101).Select(i => i.ToString()).ToList();
                colCompra.Items.AddRange(numeros.ToArray());
            }

        }

        private void dgvProductos_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvProductos.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dgvProductos.SelectedRows[0];
                // Accede a los datos de la fila seleccionada
                Producto prod = dgvProductos.Rows[dgvProductos.SelectedRows[0].Index].DataBoundItem as Producto;
                // Buscamos el producto del grid en la lista de productos
                foreach (Producto producto in listaProductos)
                {
                    if (producto.getBarCode().Equals(prod.getBarCode()))
                    {
                        actualizarImagen(producto.getRutaImagen());
                    }
                }
            }
        }
        private void actualizarImagen(string rutaImagen)
        {
            int nuevoAncho = 320; // Reemplaza con el ancho deseado
            int nuevoAlto = 320; // Reemplaza con el alto deseado
            Image imagenRedimensionada = new Bitmap(Image.FromFile(rutaImagen), new Size(nuevoAncho, nuevoAlto));
            pbImagen.Image = imagenRedimensionada;
        }

        private bool esCargaValida(string nombre,string codigo,string precio,string rutaImagen)
        {
            if (string.IsNullOrEmpty(precio))
            {
                return false;
            }
            if(string.IsNullOrEmpty(nombre) || string.IsNullOrEmpty(codigo) || string.IsNullOrEmpty(rutaImagen))
            {
                return false;
            }
            if (Double.Parse(precio)>0)
            {
                return true;
            }
            return true;

        }

        private bool esRepetido(string codigo)
        {
            foreach (Producto prod in listaProductos)
            {
                if (prod.getBarCode().Trim().Equals(codigo.Trim()))
                {
                    return true;
                }
            }
            return false;
        }

        private void obtenerProductosDeBase()
        {
            String cadenaConexion = "Database=shopping ; Data Source=localhost ; Port=3306 ; User Id=root ; Password= 123456789;";

            using (MySqlConnection connection = new MySqlConnection(cadenaConexion))
            {
                try
                {
                    connection.Open();

                    string query = "SELECT * FROM productos";
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        using (MySqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Asegúrate de adaptar esto según la estructura de tu base de datos
                                Producto producto = new Producto
                                (
                                    reader["nombre"].ToString(),
                                    reader["codigo"].ToString(),
                                    //reader["rutaImagen"].ToString(),
                                    //"",
                                    Convert.ToDouble(reader["precio"])
                                );

                                listaProductos.Add(producto);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al obtener productos: " + ex.Message);
                    // Manejar la excepción según tus necesidades
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Código que deseas ejecutar cuando el formulario se carga
            tbDeuda.Text = "0";
            tbMonto.Text = "0"; 
            obtenerProductosDeBase();
            obtenerDeudasDeBase(1);
            if (listaProductos.Count > 0)
            {
               actualizarDataGridView();
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvProductos.SelectedRows.Count > 0)
            {
                Producto productoSeleccionado = listaProductos[dgvProductos.SelectedRows[0].Index];
                eliminarProdBase(productoSeleccionado.getBarCode());
                listaProductos.RemoveAll(producto => producto.getBarCode() == productoSeleccionado.getBarCode());
                //pbImagen.Dispose();
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                File.Delete(productoSeleccionado.getRutaImagen());
                
                actualizarDataGridView();
            }
        }

        private void eliminarProdBase(string barCode)
        {
            String cadenaConexion = "Database=shopping ; Data Source=localhost ; Port=3306 ; User Id=root ; Password= 123456789;";
            try
            {
                using (MySqlConnection conexionBD = new MySqlConnection(cadenaConexion))
                {
                    conexionBD.Open();

                    // Sentencia SQL DELETE para eliminar el producto por código de barras
                    string sql = "DELETE FROM productos WHERE codigo = @Codigo";

                    using (MySqlCommand comando = new MySqlCommand(sql, conexionBD))
                    {
                        // Parámetro para el código de barras
                        comando.Parameters.AddWithValue("@Codigo", barCode);

                        // Ejecutar la sentencia DELETE
                        int filasAfectadas = comando.ExecuteNonQuery();
                    }
                }
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Error al eliminar el producto: " + ex.Message);
            }
        }

        private void dgvProductos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            // Verifica si el índice de la celda es válido y corresponde a la columna deseada
            if (e.RowIndex >= 0 && e.ColumnIndex == -1)
            {
                // Obtén el producto seleccionado
                Producto productoSeleccionado = listaProductos[e.RowIndex];

                // Abre el formulario de modificación y pasa el producto
                using (Form2 formularioModificar = new Form2(productoSeleccionado.getBarCode(), productoSeleccionado.getNombre(), productoSeleccionado.getPrecio().ToString(), productoSeleccionado.getRutaImagen()))
                {
                    formularioModificar.ShowDialog();
                    foreach (Producto producto in listaProductos)
                    {
                        if (producto.getBarCode().Equals(productoSeleccionado.getBarCode()))
                        {
                            producto.setNombre(formularioModificar.nombre);
                            producto.setRutaImagen(formularioModificar.rutaImagen);
                            producto.setPrecio(Double.Parse(formularioModificar.precio));
                            try
                            {
                                //Borrar la imagen antes de copiar la nueva
                                //pbImagen.Dispose();
                                System.GC.Collect(); 
                                System.GC.WaitForPendingFinalizers();
                                //File.Delete(subcarpetaImagenes + "\\" + formularioModificar.codigo + ".jpg");
                                File.Copy(formularioModificar.rutaImagen, subcarpetaImagenes + "\\" +       formularioModificar.codigo + ".jpg", true);
                                //actualizarImagen(subcarpetaImagenes + "\\" + formularioModificar.codigo + ".jpg");
                            }
                            catch (IOException ex)
                            {
                              //MessageBox.Show($"Error al copiar la imagen: {ex.Message}", "Error",  MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            // Actualiza el DataGridView
                            actualizarDataGridView();
                            insertarProdBase();
                        }
                    }

                }
            }
        }

        private void dgvProductos_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // Verifica si la celda que cambió pertenece a una columna de ComboBox
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0 && dgvProductos.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn)
            {
                // Obtiene el valor seleccionado en el ComboBox
                //string nuevoValor = dgvProductos.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString();

                // Realiza cualquier acción adicional que necesites con el nuevo valor
                actualizarMonto();
            }
        }

        private void dgvProductos_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            // Verifica si la celda actual es parte de una columna de ComboBox
            if (dgvProductos.IsCurrentCellDirty)
            {
                dgvProductos.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void actualizarMonto()
        {
            Double suma = 0;
            foreach (DataGridViewRow row in dgvProductos.Rows)
            {
                if (!row.IsNewRow) // Ignora la fila nueva si está presente
                {
                    // Accede al valor de la celda de la columna de ComboBox
                    DataGridViewComboBoxCell comboBoxCellCompra = row.Cells["colCompra"] as DataGridViewComboBoxCell;
                    DataGridViewCell CellPrecio = row.Cells["colPrecio"] as DataGridViewCell;

                    if (comboBoxCellCompra.Value != null && CellPrecio.Value != null)
                    {
                        string compra = comboBoxCellCompra.Value.ToString();
                        Double compraConvertido = Double.Parse(compra);
                        string precio = CellPrecio.Value.ToString();
                        Double precioConvertido = Double.Parse(precio);
                        suma += compraConvertido*precioConvertido;
                    }
                }
            }
            tbMonto.Text = suma.ToString();
        }

        private void btnHistorial_Click(object sender, EventArgs e)
        {
            obtenerDeudasDeBase(20);
        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            string mensaje = "";
            foreach (DataGridViewRow row in dgvProductos.Rows)
            {
                // Verificar si la celda de la columna "colCompra" está vacía o tiene valor 0
                DataGridViewComboBoxCell comboBoxCell = row.Cells["colCompra"] as DataGridViewComboBoxCell;

                if (comboBoxCell != null)
                {
                    if (comboBoxCell.Value != null && comboBoxCell.Value.ToString() != "0")
                    {
                        mensaje = mensaje+" "+row.Cells["colNombre"].Value.ToString()+" "+ comboBoxCell.Value.ToString()+"\n";
                    }
                }
            }
            if (!mensaje.Equals(""))
            {
                MessageBox.Show(mensaje, "¡Compra exitosa!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ningún producto seleccionado", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnAdeudar_Click(object sender, EventArgs e)
        {
            DialogResult resultado = MessageBox.Show("¿Adeudar?", "Confirmación", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            Double nuevaDeuda = Double.Parse(tbMonto.Text)+Double.Parse(tbDeuda.Text);

            if (resultado == DialogResult.Yes)
            {
                actualizarDeudaBase(nuevaDeuda);
                tbDeuda.Text = nuevaDeuda.ToString();
                MessageBox.Show("La deuda ahora es de: "+nuevaDeuda, "¡Es bueno endeudarse!", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        //cantidad, 1=trae solo la ultima. otroNumero=Trae esa cantidad para el historial
        private void obtenerDeudasDeBase(int cantidad)
        {
            String cadenaConexion = "Database=shopping ; Data Source=localhost ; Port=3306 ; User Id=root ; Password= 123456789;";

            using (MySqlConnection connection = new MySqlConnection(cadenaConexion))
            {
                try
                {
                    connection.Open();
                    if (cantidad == 1)
                    {
                        string query = "SELECT * FROM deuda ORDER BY id DESC LIMIT 1;";
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            using (MySqlDataReader reader = command.ExecuteReader())
                            {
                                reader.Read();
                                tbDeuda.Text = reader["monto"].ToString();
                            }
                        }
                    }                
                    else
                    {
                        string historial = "";
                        string query = $"SELECT monto,DATE(fecha) as fecha FROM deuda ORDER BY id DESC LIMIT {cantidad};";
                        using (MySqlCommand command = new MySqlCommand(query, connection))
                        {
                            using (MySqlDataReader reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    string dateFormat = reader["fecha"].ToString().Substring(0, 10);
                                    string montoFormat = reader["monto"].ToString();

                                    // Ajusta el ancho de la columna del monto según tus necesidades
                                    int anchoColumnaMonto = 15;

                                    // Formatea la cadena para alinear correctamente las columnas
                                    string fila = $"{montoFormat.PadRight(anchoColumnaMonto)}\t{dateFormat}";

                                    historial = historial + fila + Environment.NewLine;
                                }
                            }
                        }
                        MessageBox.Show(historial, "Historial de deudas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error al obtener productos: " + ex.Message);
                }
            }
        }

        private void actualizarDeudaBase(Double monto)
        {
            string query = "INSERT INTO deuda (Monto, Fecha) VALUES (@Monto, NOW())";
            String cadenaConexion = "Database=shopping ; Data Source=localhost ; Port=3306 ; User Id=root ; Password= 123456789;";

            using (MySqlConnection connection = new MySqlConnection(cadenaConexion))
            {
                try
                {
                    connection.Open();
                    {
                    using (MySqlCommand command = new MySqlCommand(query, connection))
                    {
                        command.Parameters.AddWithValue("@Monto", monto);
                        int filasAfectadas = command.ExecuteNonQuery();
                        if (filasAfectadas > 0)
                        {
                            Console.WriteLine("Registro insertado correctamente.");
                        }
                        else
                        {
                            Console.WriteLine("No se pudo insertar el registro.");
                        }
                    }
                    }
                
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error: " + ex.Message);
                }
            }
        }

        private void btnCdeuda_Click(object sender, EventArgs e)
        {
            actualizarDeudaBase(0);
            tbDeuda.Text = "0";
            MessageBox.Show("Deuda cancelada", "Felicidades", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            // Validar la entrada como un número
            if (int.TryParse(tbPaga.Text, out int numero) )
            {
                Double nuevaDeuda = Double.Parse(tbDeuda.Text) - Double.Parse(tbPaga.Text);
                actualizarDeudaBase(nuevaDeuda);
                tbDeuda.Text = nuevaDeuda.ToString();
                tbPaga.Text = "";
                MessageBox.Show($"Deuda actual: {nuevaDeuda}", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Por favor, ingrese un número válido.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
